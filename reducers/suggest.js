var suggestActions = function(state ={}, action) {

    var obj,
        selIndex,
        max,
        station;

    switch (action.type) {

        case 'SELECT_STATION':
            obj =  Object.assign({}, state);
            obj[action.instance].station    =  action.station;
            obj[action.instance].selIndex   =  action.selIndex;
            obj[action.instance].inpValue   =  obj[action.instance].station.name;
            obj[action.instance].listDpl    =  false;
            return obj;

        case 'SET_INPUT_VALUE':

            obj =  Object.assign({}, state);
            obj[action.instance].inpValue   =  action.inpValue;

            Object.keys(obj).map( (instance) => {
                obj[instance].listDpl       =  false;
            })
            if(obj[action.instance].station) {
                obj[action.instance].listDpl    =  true;
            }
            return obj;

        case 'SET_LIST_DPL':

            obj =  Object.assign({}, state);
            obj[action.instance].listDpl    =  action.listDpl;
            if(!obj[action.instance].listDpl) {
                obj[action.instance].inpValue   =  obj[action.instance].station.name || false;
            }

            return obj;

        case 'HIDE_LIST':

            obj =  Object.assign({}, state);

            Object.keys(obj).map( (instance) => {
                obj[instance].listDpl       =  false;
                obj[instance].inpValue      =  obj[instance].station.name || false;
            })

            return obj;

        case 'SET_SELECTED_INDEX':

            obj =  Object.assign({}, state);

            max         =  obj[action.instance].response.length;
            selIndex    =  obj[action.instance].selIndex;

            if(action.modus === 'up') {
                selIndex =  (selIndex -1 > -1)? selIndex -=1 : max -1;
            } else {
                selIndex =  (selIndex +1 < max)? selIndex +=1 : 0;
            }
            obj[action.instance].selIndex   =  selIndex;
            return obj;

        case 'SELECT_STATION_BY_INDEX':

            if(!state[action.instance].response) {
                return state
            }

            obj =  Object.assign({}, state);
            selIndex    =  obj[action.instance].selIndex;
            station     =  obj[action.instance].response[selIndex] || obj[action.instance].response[0];

            obj[action.instance].station    =  station
            obj[action.instance].inpValue   =  obj[action.instance].station.name
            obj[action.instance].listDpl    =  false;

            return obj;


        case 'SUGGEST_RESPONSE':

            obj =  Object.assign({}, state);
            obj[action.instance].search     =  action.search;
            obj[action.instance].response   =  action.response;
            obj[action.instance].lastUpd    =  new Date().getTime();
            obj[action.instance].listDpl    =  true;
            if(!action.isFetching) {
                // remove one of the multiple request after response
                obj[action.instance].fetching.splice(0, 1);
            }
            return obj;

        case 'SUGGEST_REQUEST':

            obj =  Object.assign({}, state);
            obj[action.instance].inpValue   =  action.inpValue;
            if(action.isFetching) {
                // store multiple requests
                obj[action.instance].fetching.push(action.inpValue);
            }
            return obj;

        default:
            return state
    }
}

module.exports =  suggestActions;
