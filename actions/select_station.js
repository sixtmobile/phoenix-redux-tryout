export const select_station = (instance, station, selIndex) => {
    return {
        type: 'SELECT_STATION',
        instance: instance,
        station: station,
        selIndex: selIndex
    }
}
