export const set_selected_index = (instance, modus) => {
    return {
        type: 'SET_SELECTED_INDEX',
        instance: instance,
        modus: modus
    }
}
