export const suggest_request_exec = (instance, inpValue, isFetching) => {
    return {
        type: 'SUGGEST_REQUEST',
        instance: instance,
        inpValue: inpValue,
        isFetching: isFetching
    }
}

export const suggest_response = (instance, response, search, isFetching) => {
    return {
        type: 'SUGGEST_RESPONSE',
        instance: instance,
        response: response,
        search: search,
        isFetching: isFetching
    }
}

export const suggest_request = (instance, inpValue, baseUrl) => {

    return dispatch => {

        var isFetching =  true;
        if(inpValue.length < 3) {
            isFetching =  false;
        }

        dispatch( suggest_request_exec(instance, inpValue, isFetching) );
        if(!isFetching) {
            return false;
        }

        var url     =  baseUrl + "?q=" + inpValue;
        var xhttp   =  new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (xhttp.readyState == 4 && xhttp.status == 200) {

                    var responseObj     =  JSON.parse(xhttp.responseText);
                    var stationArray    =  [];
                    Object.keys(responseObj).map( (cat) => {
                        responseObj[cat].map((station, index) => {
                            station['category'] =  cat;
                            stationArray.push(station);
                        })
                    })
                    dispatch( suggest_response(instance, stationArray, inpValue, false) );
                }
            };
            xhttp.open("GET", url, true);
            xhttp.send();
    }
}

