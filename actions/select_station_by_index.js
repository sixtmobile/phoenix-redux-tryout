export const select_station_by_index = (instance) => {
    return {
        type: 'SELECT_STATION_BY_INDEX',
        instance: instance
    }
}
