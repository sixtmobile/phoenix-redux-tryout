
"use strict"

var googleMaps =  {

    config: {
        mapContainerId:     'googleMap',
        locations:          [{lat:50.110924,lng:8.682127,infos:{title:'Frankfurt'}}],
        apiUrl:             '\/\/maps.googleapis.com/maps/api/js',
        apiKey:             'AIzaSyAEguLFvjld_xq1dQUBjHkZtpUNgpPjh10',
        markers:            []
    },

    googleMap:  false,
    googleMapBounds: false,

    setCurrentPosition( location ) {
        this.config.locations =  [{
            lat: location.lat,
            lng: location.lng,
            title: location.infos.title
        }];
        if(!this.googleMap) {
            return false;
        }
        this.deleteMarkersFromGoogleMap();
        this.moveGoogleMapToLatLng( location );
    },

    setCurrentPositions( locations ) {
        this.config.locations =  locations;
        if(!this.googleMap) {
            return false;
        }
        this.deleteMarkersFromGoogleMap();
        this.setGoogleMapBoundsByLatLng( locations );
    },

    /**
     * use html5 geolocation to get an initial position
     *
     */
    getGeolocationFromClient() {
        var obj = this;
        navigator.geolocation.getCurrentPosition(function(position){
            obj.config.locations            =  [{
                lat: position.coords.latitude,
                lng: position.coords.longitude,
                infos: {title: 'Aktuelle Position'}
            }];
            obj.moveGoogleMapToLatLng( obj.config.locations[0] );
        }, function () {
            // fail
        });
    },

    initializeGoogleMap() {
        var obj     =  this;
        var latLng  =  new google.maps.LatLng( this.config.locations[0].lat, this.config.locations[0].lng );
        var opt     =  {
            zoom: 12,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false,
            styles: [{
                featureType: "poi.business",
                elementType: "labels",
                stylers: [ {visibility: "off"} ]
            }]
        };
        this.googleMap =  new google.maps.Map( document.getElementById(this.config.mapContainerId), opt );
        this.getGeolocationFromClient();

        // add event-handler to display marker after bounds changed
        google.maps.event.addListener( this.googleMap, "bounds_changed", function() {
            obj.handleGoogleMapBoundsChange();
        } );
    },

    moveGoogleMapToLatLng( location ) {
        var latLng =  new google.maps.LatLng(location.lat, location.lng);
        this.googleMap.panTo( latLng );
        this.setGoogleMapMarker( location );
    },

    setGoogleMapBoundsByLatLng( locations ) {

        var googleMapBounds =  new google.maps.LatLngBounds();

        // loop all positions to extend map's bounds
        locations.map( location => {
            let latLng =  new google.maps.LatLng( location.lat, location.lng );
            googleMapBounds.extend( latLng );

            this.setGoogleMapMarker( location );
        } )
        // move map within bounds
        this.googleMap.fitBounds( googleMapBounds );

        // if nearby and result pos matches, zoom is to high
        // set zoom a little lower
        var zoom =  this.googleMap.getZoom();
        if (zoom > 17) {
             this.googleMap.setZoom(17);
        }
    },

    setGoogleMapMarker( location ) {
        var latLng =  new google.maps.LatLng(location.lat, location.lng);
        var marker =  new google.maps.Marker( {
            position: latLng,
            map: this.googleMap,
            title: location.infos.title
        } );

        var click =  location.click || false;
        var obj =  this;
        if(click) {
            //google.maps.event.addListener(marker, 'click', e=>{click(e, location.infos)});
            google.maps.event.addListener(marker, 'click', e=>{this.displayGoogleMarkerInfo(marker, location.infos);return true});
        }
        this.config.markers.push(marker);
    },

    deleteMarkersFromGoogleMap() {
        this.config.markers.map(marker => {
            marker.setMap(null)
        })
    },

    handleGoogleMapBoundsChange() {
        var location =  this.config.locations[0];
        this.setGoogleMapMarker( location );
    },

    displayGoogleMarkerInfo(marker, infos) {
        var infowindow = new google.maps.InfoWindow({
            content: this.createGoogleMarkerInfo(infos)
        });
        infowindow.open(this.googleMap, marker);
    },

    createGoogleMarkerInfo(infos) {
        var str =   '<div class="suggestGoogleInfoContent">';
            str +=  '<h3>' + infos.name + '<\/h3>';

            if(infos.address) {
                str +=  '<div>' + infos.address.street + ', ' + infos.address.postcode + ' ' + infos.address.city + '<\/div>';
            }

            if(infos.openingHours) {
                str += '<div><table>';
                infos.openingHours.map(item => {
                    str += '<tr><td>' + item.day + '<\/td><td>';
                    item.times.map( time => {
                        str += 'open: ' + time.open + '<br>';
                        str += 'close: ' + time.close +'<br>';
                    })
                    str += '<\/td><\/tr>';
                })
                str += '<\/table><\/div>';
            }


            str +=  '<\/div>'
        return str;
    },

    init() {

        // execute clientside only
        if (typeof window === 'undefined') {
            return false;
        }

        var obj                 =  this;
        window.startGoogleMap   =  function() {obj.initializeGoogleMap()};

        var head                =  document.getElementsByTagName('head')[0];
        var googleUrl           =  (self.location.protocol || 'http:') + this.config.apiUrl + '?key=' + this.config.apiKey + '&callback=startGoogleMap';
        var googleScript        =  document.createElement('script');
            googleScript.src    =  googleUrl;
            googleScript.type   =  'text/javascript';
            googleScript.async  =  true;
            googleScript.defer  =  true;
            head.appendChild( googleScript );
    }
}

module.exports =  googleMaps


