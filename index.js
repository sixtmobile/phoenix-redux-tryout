import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import storeObj from './store/storeObj'
import AppBox from './components/AppBox'

const store = configureStore( storeObj );
/*
store.subscribe(() => {
    console.info("-------- store ---------");
    console.info(store.getState())
    console.info("-------- store ---------");
})
*/


render(
    <Provider store={store}>
        <AppBox />
    </Provider>,
    document.getElementById('sixtApp')
)