import React, { PropTypes } from 'react'
import SuggestFormBox from './suggest/SuggestFormBox'
import GoogleMapBox from './GoogleMapBox'

let AppBox = () => {
    return (
        <div className="appWrapper">
            <GoogleMapBox />
            <div className="appContent">
                <SuggestFormBox  />
            </div>
        </div>
    )
}

export default AppBox