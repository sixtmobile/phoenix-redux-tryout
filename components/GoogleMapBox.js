import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux'
import googleMap from '../modules/googleMap'

class GoogleMapBox extends Component {

    constructor (props) {
        super(props)
    }

    componentDidMount() {
        googleMap.init()
    }

    handleMarkerClick(e, infos) {
        alert(title)
    }

    shouldComponentUpdate(nextProps) {

        let locations =  [];
        if(nextProps.pickupStation) {
            locations.push( {lat:nextProps.pickupStation.address.coordinates.lat, lng:nextProps.pickupStation.address.coordinates.lon, infos:nextProps.pickupStation, click:true} )
        }
        if(nextProps.returnStation) {
            locations.push( {lat:nextProps.returnStation.address.coordinates.lat, lng:nextProps.returnStation.address.coordinates.lon, infos:nextProps.returnStation, click:true} )
        }
        googleMap.setCurrentPositions( locations )

        return false
    }

    render() {
        return (
            <div id="googleMap" className="googleMap"></div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        pickupStation: state.suggest.pickupData.station,
        returnStation: state.suggest.returnData.station
    }
}

GoogleMapBox = connect(mapStateToProps)(GoogleMapBox)

export default GoogleMapBox