import React, { PropTypes, Component } from 'react'
import SuggestInput from './SuggestInput'
import SuggestList from './SuggestList'

let InstanceBox = ({instance}) => {
    return (
        <div className="instanceBox">
            <SuggestInput instance={instance} />
            <SuggestList instance={instance} />
        </div>
    )
}

export default InstanceBox
