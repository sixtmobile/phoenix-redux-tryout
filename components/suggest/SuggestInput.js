import React, { PropTypes, Component } from 'react'
import { connect } from 'react-redux'
import { set_input_value } from '../../actions/set_input_value'
import { suggest_request } from '../../actions/suggest_request'
import { set_selected_index } from '../../actions/set_selected_index'
import { select_station_by_index } from '../../actions/select_station_by_index'

class SuggestInput extends Component {

    constructor (props) {
        super(props)
        //this.baseUrl        =  "https://app.sixt.de/php/mobilews/v4/stationsuggestion";
        this.baseUrl        =  "https://rentalcars.sixt.com/api/v0/stations";
        this.searchLabel    =  (this.props.instance === "pickupData")? "Pickup" : "Return"
        this.stationSearch  =  false
    }

    handleStationSearch(e, stationSearch) {
        this.props.dispatch( suggest_request(this.props.instance, stationSearch.value, this.baseUrl) );
    }

    handleKeyDown(e) {
        if(!this.props.listDpl) {
            return true;
        }
        switch(e.which) {
            case 38:
                this.props.dispatch( set_selected_index(this.props.instance, 'up') );
                break;
            case 40:
                this.props.dispatch( set_selected_index(this.props.instance, 'down') );
                break;
            case 13:
                this.props.dispatch( select_station_by_index(this.props.instance) );
                break;
        }
    }

    handleSearchClick() {
        this.props.dispatch( set_input_value(this.props.instance, false) )
    }

    render() {
        return (
            <div>
                <p>
                    <label>{this.searchLabel}</label>
                    <input
                        ref={node => {this.stationSearch = node}}
                        type="text"
                        placeholder="Desination"
                        onInput={(e)=>{this.handleStationSearch(e, this.stationSearch)}}
                        onClick={(e)=>{this.handleSearchClick(e)}}
                        value={this.props.inpValue? this.props.inpValue : ""}
                        onKeyDown={(e)=>{this.handleKeyDown(e)}}
                    />
                </p>
            </div>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    let suggestData =  state.suggest[ownProps.instance];
    return { inpValue:suggestData.inpValue,listDpl:suggestData.listDpl }
}

SuggestInput = connect(mapStateToProps)(SuggestInput)

export default SuggestInput
