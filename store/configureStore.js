import { createStore, compose, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import reducer from '../reducers'

export default function configureStore(initialState) {
    const store = createStore(reducer, initialState, compose(
        applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f => f
    ));
    return store;
}
