var storeObj =  {
    suggest: {
        pickupData: {
            search:     false,
            response:   false,
            station:    false,
            inpValue:   false,
            listDpl:    false,
            lastUpd:    false,
            fetching:   [],
            selIndex:   0
        },
        returnData: {
            search:     false,
            response:   false,
            station:    false,
            inpValue:   false,
            listDpl:    false,
            lastUpd:    false,
            fetching:   [],
            selIndex:   0
        }
    }
}

module.exports =  storeObj